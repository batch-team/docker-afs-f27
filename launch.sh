#!/bin/bash

. /etc/sysconfig/openafs
echo cern.ch > /usr/vice/etc/ThisCell
/bin/test -f /lib/modules/`uname -r`/openafs.ko || /bin/ln -s /hacks/lib/modules/*/extra/openafs/openafs.ko /lib/modules/`uname -r`/
depmod -a
modprobe openafs
/usr/vice/etc/afsd $AFSD_ARGS
/usr/bin/afs-setserverprefs

