FROM fedora:27

MAINTAINER Ben Jones <ben.dylan.jones@cern.ch>


RUN yum install -y dnf-plugins-core 
RUN dnf -y copr enable jsbillings/openafs
RUN dnf update -y
RUN dnf download openafs-client 
RUN rpm -i --nodeps ./openafs-client*
RUN /bin/mkdir /hacks
RUN dnf install -y cpio kmod
RUN dnf download kmod-openafs
RUN /bin/mv ./kmod-openafs* /hacks/
RUN cd /hacks && rpm2cpio ./kmod-openafs* | cpio -ind
RUN yum install -y krb5-workstation \
		   openafs-krb5     


ADD sysconfig/openafs /etc/sysconfig/openafs
ADD CellServDB /usr/vice/etc/
ADD CellServDB.dist /usr/vice/etc/
ADD CellServDB.local /usr/vice/etc/
ADD ThisCell /usr/vice/etc/
RUN /bin/chmod 0644 /usr/vice/etc/ThisCell
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.dist
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.local
RUN /bin/mkdir -p /afs

ADD afs-setserverprefs /usr/bin/
ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
